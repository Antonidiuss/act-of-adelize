﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Block : ScriptableObject 
{
    public string _name;
    public string _description;

    public Sprite _sprite;
    public float _maxDur;
    


    public abstract void HitBlock(float power);

    public abstract void OnBlockDestroy();

}
