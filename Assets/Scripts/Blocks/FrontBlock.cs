﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Block", menuName = "Blocks/Solid")]
public class FrontBlock : Block
{
    public override void HitBlock(float power)
    {

    }

    public override void OnBlockDestroy()
    {
        
    }
}
