﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MVVMBlock : MonoBehaviour
{
    public ChunkGO chunkGO;
    public BlockItem block;

    public float CurrentDur;
    public int _x, _y; 

    public SpriteRenderer spriteRenderer;
    public SpriteRenderer destructionAnimator;

    public Sprite destr1, destr2, destr3;
    WorldScr world;
    
    public void Hit(float power)
    {
        CurrentDur -= power;
        if (CurrentDur <= 0)
        {
            OnBlockDestroy();
        }
    }

    public void Start()
    {
        world = GameObject.Find("World").GetComponent<WorldScr>();
        CurrentDur = block.Defense;
        spriteRenderer.sprite = block.Sprite;
    }

    public void OnBlockDestroy()
    {
        //block.OnBlockDestroy();
        world.SpawnItem(block, (int)transform.position.x, (int)transform.position.y);
        chunkGO._chunk.blockDatas[_x, _y] = new BlockItem();
        chunkGO._chunk.blockDatas[_x, _y].Id = -1;
        Destroy(gameObject);
    }

    public void Update()
    {
        
    }

    public void LateUpdate()
    {
        if (CurrentDur > 0.75 * block.Defense)
        {
            destructionAnimator.sprite = null;
        }
        else if (CurrentDur > 0.5 * block.Defense && CurrentDur < 0.75 * block.Defense)
        {
            destructionAnimator.sprite = destr1;
        }
        else if (CurrentDur > 0.25 * block.Defense && CurrentDur < 0.5 * block.Defense)
        {
            destructionAnimator.sprite = destr2;
        }
        else if (CurrentDur < 0.25 * block.Defense)
        {
            destructionAnimator.sprite = destr3;
        }
    }
}
