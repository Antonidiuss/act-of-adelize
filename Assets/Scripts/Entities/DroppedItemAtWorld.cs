﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItemAtWorld : MonoBehaviour
{
    public Item item;

    

    private void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        Physics.IgnoreLayerCollision(13, 13);

        GetComponent<SpriteRenderer>().sprite = item.Sprite;
    }
    
    void Update()
    {
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, 10f);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            if (hitColliders[i].tag == "Player")
            {
                if (Mathf.Sqrt(Mathf.Pow(transform.position.x - hitColliders[i].transform.position.x,2)+Mathf.Pow(transform.position.y - hitColliders[i].transform.position.y,2)) < 3)
                {
                    hitColliders[i].transform.GetChild(4).GetComponent<Inventory>().AddItem(item);
                    Destroy(gameObject);
                    break;
                }
            }
        }
    }
}
