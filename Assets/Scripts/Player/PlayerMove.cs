﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour
{
    public CharacterController2D controller;
    public float runSpeed;
    public Animator animator;
    public Grid grid;
    public MVVMBlock selectedBlock;
    public WorldScr worldScr;

    public Block blockToPlace;
    Inventory inv;


    [Range(0, 20)] [SerializeField] private float m_DestroySpeed = 5f;

    bool jump, crouch, run, attack = false;
    float horizontalMove = 0f;

    private void Start()
    {
        inv = GameObject.Find("Inventory").GetComponent<Inventory>();

        
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

        RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject.GetComponent<MVVMBlock>() != null)
            {
                selectedBlock = hit.collider.gameObject.GetComponent<MVVMBlock>();
            }
        }




        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            animator.SetBool("Jump", true);
        }


        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
            animator.SetBool("Crawl", true);
        }
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }

        if (Input.GetButtonDown("Run"))
        {
            run = true;
            animator.SetBool("Run", true);
        }
        else if (Input.GetButtonUp("Run"))
        {
            run = false;
            animator.SetBool("Run", false);
        }


        if (Input.GetButtonDown("Fire1"))
        {
            attack = true;
            animator.SetBool("Attack", true);
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            attack = false;
            animator.SetBool("Attack", false);
        }



        if (Input.GetButtonDown("Fire2"))
        {
            if (inv.items[0].Id != -1)
            {
                Item item = inv.items[0];
                if (item.Blockable)
                {
                    if (worldScr.IsBlockCanBePlaced(grid.WorldToCell(mousePos).x, grid.WorldToCell(mousePos).y))
                    {
                        worldScr.PlaceBlockAt(item.Id, grid.WorldToCell(mousePos).x, grid.WorldToCell(mousePos).y);

                        inv.slots[0].transform.GetChild(0).GetComponent<ItemData>().amount--;
                        inv.slots[0].transform.GetChild(0).GetChild(1).GetComponent<Text>().text = (inv.slots[0].transform.GetChild(0).GetComponent<ItemData>().amount+1).ToString();
                        if (inv.slots[0].transform.GetChild(0).GetComponent<ItemData>().amount < 0)
                        {
                            inv.items[0] = new Item();
                            Destroy(inv.slots[0].transform.GetChild(0).gameObject);
                        }
                    }
                }
            }
        }

        
    }

    public void OnLanding()
    {
        animator.SetBool("Jump", false);
    }

    public void OnCrouchOut()
    {
        animator.SetBool("Crawl", false);

    }


    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.deltaTime, crouch, run, jump);
        jump = false;

        if (attack)
        {
            if (selectedBlock != null)
            {
                selectedBlock.Hit(m_DestroySpeed * Time.deltaTime);
            }
        }
    }
}
