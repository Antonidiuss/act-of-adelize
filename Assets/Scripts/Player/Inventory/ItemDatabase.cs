﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Linq;
using LitJson;

public class ItemDatabase : MonoBehaviour
{
    private List<Item> database = new List<Item>();
    private JsonData itemData;

    //public static class JsonHelper
    //{
    //    public static T[] FromJson<T>(string json)
    //    {
    //        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
    //        return wrapper.Items;
    //    }

    //    public static string ToJson<T>(T[] array)
    //    {
    //        Wrapper<T> wrapper = new Wrapper<T>();
    //        wrapper.Items = array;
    //        return JsonUtility.ToJson(wrapper);
    //    }

    //    public static string ToJson<T>(T[] array, bool prettyPrint)
    //    {
    //        Wrapper<T> wrapper = new Wrapper<T>();
    //        wrapper.Items = array;
    //        return JsonUtility.ToJson(wrapper, prettyPrint);
    //    }

    //    [Serializable]
    //    private class Wrapper<T>
    //    {
    //        public T[] Items;
    //    }
    //}

    


    private void Start()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/Scripts/Player/Inventory/Items.Json"));

        

        //Create the database from the json file
        ConstructItemDatabase();
        //Debug.Log(database[0].Id);
        //Debug.Log(database[1].Id);
        //Debug.Log(database[2].Id);
        //Debug.Log(FetchItemByID(0).Description);
    }

    public Item FetchItemByID(int id)
    {
        for (int i = 0; i < database.Count(); i++)
            if (database[i].Id == id)
                return database[i];
        return null;
    }

    public BlockItem FetchBlockItemByID(int id)
    {
        for (int i = 0; i < database.Count(); i++)
            if (database[i].Id == id)
                return itemToBlockitem(database[i]);
        return null;
    }

    private BlockItem itemToBlockitem(Item v)
    {
        BlockItem block = new BlockItem(v.Id, v.Blockable, v.Title, v.Value, v.Power, v.Defense, v.Vitality, v.Description, v.Stackable, v.Rarity, v.Slug, v.Sprite);
        return block;
    }

    //void SaveDatabase()
    //{
    //    Item[] itemInstance = new Item[database.Count()];
    //    for (int i = 0; i < database.Count(); i++)
    //    {
    //        itemInstance[i] = database[i];
    //    }

    //    //Convert to Jason
    //    string itemsToJason = JsonHelper.ToJson(itemInstance);
    //    string path = Application.dataPath + "/Scripts/Player/Inventory/Items.Json";
    //    File.WriteAllText(path, itemsToJason.ToString());
    //    Debug.Log(itemsToJason);
    //}

    void ConstructItemDatabase()
    {

        Sprite[] BlockAtlas = Resources.LoadAll<Sprite>("Sprites/Terraria/Tileset");
        

#if UNITY_EDITOR
        //        string path = Application.dataPath + "/Scripts/Player/Inventory/Items.Json";
        //        string jsonString = File.ReadAllText(path);
        //Item[] items = JsonHelper.FromJson<Item>(jsonString);



        for (int j = 0; j < itemData.Count; j++)
        {
            if ((bool)itemData[j]["Blockable"])
            {

                BlockItem itemConstruct = new BlockItem((int)itemData[j]["Id"], (bool)itemData[j]["Blockable"], itemData[j]["Title"].ToString(), (int)itemData[j]["Value"],
                    (int)itemData[j]["Power"], (int)itemData[j]["Defense"], (int)itemData[j]["Vitality"], itemData[j]["Description"].ToString(), (bool)itemData[j]["Stackable"],
                    (int)itemData[j]["Rarity"], itemData[j]["Slug"].ToString(), BlockAtlas.Single(s => s.name == itemData[j]["Slug"].ToString()));
                database.Add(itemConstruct);
            }
            else
            {
                ThingItem itemConstruct = new ThingItem((int)itemData[j]["Id"], (bool)itemData[j]["Blockable"], itemData[j]["Title"].ToString(), (int)itemData[j]["Value"],
                    (int)itemData[j]["Power"], (int)itemData[j]["Defense"], (int)itemData[j]["Vitality"], itemData[j]["Description"].ToString(), (bool)itemData[j]["Stackable"],
                    (int)itemData[j]["Rarity"], itemData[j]["Slug"].ToString(), Resources.Load<Sprite>("Sprites/Items/" + itemData[j]["Slug"].ToString()));
                database.Add(itemConstruct);
            }
        }

#elif UNITY_ANDROID
        string path = "jar:file://" + Application.dataPath + "!/assets/Items.json";
        StartCoroutine(GetDataInAndroid(path));
#endif
    }

    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.S)) SaveDatabase();
    //}

    //IEnumerator GetDataInAndroid(string url)
    //{
    //    WWW www = new WWW(url);
    //    yield return www;
    //    if (www.text != null)
    //    {
    //        string dataAsJson = www.text;
    //        Item[] items = JsonHelper.FromJson<Item>(dataAsJson);

    //        for (int i = 0; i < items.Count(); i++)
    //        {
    //            database.Add(items[i]);
    //        }
    //    }
    //    else
    //    {
    //        Debug.LogError("Can not load game data! Android");
    //    }
    //}
}