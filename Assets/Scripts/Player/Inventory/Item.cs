﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class Item : ScriptableObject
{
    public int Id;
    public bool Blockable;
    public string Title;
    public int Value;
    public int Power;
    public int Defense;
    public int Vitality;
    public string Description;
    public bool Stackable;
    public int Rarity;
    public string Slug;
    public Sprite Sprite;

    public Item()
    {
        this.Id = -1;
    }



    public virtual void OnUse()
    {

    }
}


public class BlockItem : Item
{
    public BlockItem(int id, bool blockable, string title, int value, int power, int defense, int vitality, string description, bool stackable, int rarity, string slug, Sprite slugSprite)
    {
        this.Id = id;
        this.Blockable = blockable;
        this.Title = title;
        this.Value = value;
        this.Power = power;
        this.Defense = defense;
        this.Vitality = vitality;
        this.Description = description;
        this.Stackable = stackable;
        this.Rarity = rarity;
        this.Slug = slug;
        this.Sprite = slugSprite;
    }

    public BlockItem()
    {
        this.Id = -1;
        
    }

    public override void OnUse()
    {

    }

   
}

public class ThingItem : Item
{
    public ThingItem(int id, bool blockable, string title, int value, int power, int defense, int vitality, string description, bool stackable, int rarity, string slug, Sprite slugSprite)
    {
        this.Id = id;
        this.Blockable = blockable;
        this.Title = title;
        this.Value = value;
        this.Power = power;
        this.Defense = defense;
        this.Vitality = vitality;
        this.Description = description;
        this.Stackable = stackable;
        this.Rarity = rarity;
        this.Slug = slug;
        this.Sprite = slugSprite;
    }

    public override void OnUse()
    {

    }
}
