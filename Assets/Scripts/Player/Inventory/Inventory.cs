﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    GameObject inventoryPanel;
    GameObject slotPanel;
    public GameObject inventorySlot;
    public GameObject inventoryItem;
    ItemDatabase database;
    public List<Item> items = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();

    int slotAmount;

    private void Start()
    {
        database = GetComponent<ItemDatabase>();
        slotAmount = 8;
        inventoryPanel = GameObject.Find("Inventory Panel");
        slotPanel = inventoryPanel.transform.Find("Slot Panel").gameObject;

        for (int i = 0; i < slotAmount; i++)
        {
            items.Add(new Item());
            slots.Add(Instantiate(inventorySlot));
            slots[i].transform.SetParent(slotPanel.transform);
            slots[i].GetComponent<Slot>().id = i;
        }
        
    }

    public void AddItem(Item item)
    {
        AddItem(item.Id);
    }


    public void AddItem(int id)
    {
        Item itemToAdd = database.FetchItemByID(id);

        if (itemToAdd.Stackable && IsInInventory(itemToAdd))
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Id == id)
                {
                    ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();
                    data.amount++;
                    data.transform.GetChild(1).GetComponent<Text>().text = (data.amount+1).ToString();
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < items.Count; i++)
            {
                //Find an empty slot
                if (items[i].Id == -1)
                {
                    items[i] = itemToAdd;
                    GameObject itemObj = Instantiate(inventoryItem);
                    itemObj.GetComponent<ItemData>().item = itemToAdd;
                    itemObj.GetComponent<ItemData>().slot = i;
                    itemObj.transform.SetParent(slots[i].transform);
                    itemObj.transform.position = Vector2.zero;
                    
                    itemObj.GetComponent<Image>().sprite = itemToAdd.Sprite;
                    itemObj.transform.GetChild(1).GetComponent<Text>().text = "1";
                    itemObj.name = itemToAdd.Title;
                    break;
                }
            }
        }
    }

    public void RefreshAmmount()
    {
        for (int i = 0; i < slotAmount; i++)
        { 
            if (items[i].Id != -1)
                slots[i].transform.GetChild(0).GetChild(1).GetComponent<Text>().text = (slots[i].transform.GetChild(0).GetComponent<ItemData>().amount + 1).ToString();
        }
    }

    bool IsInInventory(Item item)
    {
        for (int i = 0; i < items.Count; i++)
            if (items[i].Id == item.Id)
                return true;
        return false;
    }
}
