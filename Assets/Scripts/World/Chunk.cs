﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk 
{
    public static int size = 8;

    public int _x, _y;

    public BlockItem[,] blockDatas;

    void Awake()
    {
        
    }


    //public void InitChunk()
    //{
    //    blockDatas = new BlockData[size, size];

    //}

    public BlockItem[,] GetBlockData()
    {
        return blockDatas;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
