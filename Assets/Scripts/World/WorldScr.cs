﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldScr : MonoBehaviour
{ //string name, int size, float seed, float smoothness, float heightMultip, int heightAddition
    [SerializeField] private string w_name = "Test World";
    [SerializeField] private int w_size = 8;
    [SerializeField] private float w_seed = 15;
    [Range(0, 50)] [SerializeField] private float w_smoothness = 5f;          
    [Range(0, 40)] [SerializeField] private float w_heightMultip = 2;
    [Range(0, 30)] [SerializeField] private int w_heightAddition = 6;

    public int RenderDistnace = 3;
    public GameObject ChunkGOPrefab, DropItemPrefab;
    public World world;

    public MVVMBlock MVVMBlockPrefab;

    public Block Grass, Dirt, Stone, Air;

    Dictionary<Vector2Int, ChunkGO> ChunkMap;


    WorldGen worldGen = new WorldGen();
    ItemDatabase database;


    private void Awake()
    {
        ChunkMap = new Dictionary<Vector2Int, ChunkGO>();
        worldGen.InitGen();
        worldGen.Grass = Grass;
        worldGen.Stone = Stone;
        worldGen.Dirt = Dirt;
        worldGen.Air = Air;
        
    }


    // Start is called before the first frame update
    void Start()
    {
        database = GameObject.Find("Inventory").GetComponent<ItemDatabase>();
        world = worldGen.GenerateWorld(w_name, w_size, w_seed, w_smoothness, w_heightMultip, w_heightAddition);
    }

    // Update is called once per frame
    void Update()
    {
        FindChunkToLoad();
        DeleteChunksAt();
    }

    void FindChunkToLoad()
    {
        int xPos = (int)transform.position.x;
        int yPos = (int)transform.position.y;

        for (int i = xPos - Chunk.size; i < xPos + (2 * Chunk.size); i += Chunk.size)
        {
            for (int j = yPos - Chunk.size; j < yPos + (2 * Chunk.size); j += Chunk.size)
            {
                MakeChunkAt(i, j);
            }
        }
    }

    void LoadChunks()
    {

    }

    void DeleteChunksAt()
    {
        List<ChunkGO> chunkGOs = new List<ChunkGO>(ChunkMap.Values);
        Queue<ChunkGO> DeletedChunks = new Queue<ChunkGO>();

        for (int i = 0; i < chunkGOs.Count; i++)
        {
            float distance = Vector3.Distance(transform.position, chunkGOs[i].transform.position);

            if (distance > RenderDistnace * Chunk.size)
            {
                DeletedChunks.Enqueue(chunkGOs[i]);
            }
        }

        while (DeletedChunks.Count > 0)
        {
            ChunkGO chunkGO = DeletedChunks.Dequeue();
            ChunkMap.Remove(new Vector2Int((int)chunkGO.transform.position.x / Chunk.size, (int)chunkGO.transform.position.y / Chunk.size));
            Destroy(chunkGO.gameObject);
        }
    }

    void MakeChunkAt(int x, int y)
    {
        x = Mathf.FloorToInt(x / (float)Chunk.size);
        y = Mathf.FloorToInt(y / (float)Chunk.size);

        try
        {
            if (ChunkMap.ContainsKey(new Vector2Int(x, y)) == false && world.map[x, y] != null)
            {
                GameObject go = Instantiate(ChunkGOPrefab, new Vector3(x * Chunk.size, y * Chunk.size, 0f), Quaternion.identity);
                ChunkGO chunkGO = go.GetComponent<ChunkGO>();
                ChunkMap.Add(new Vector2Int(x, y), chunkGO);
                chunkGO._chunk = world.map[x, y];
            }
        }
        catch (System.IndexOutOfRangeException ex)
        {
            
        }
    }

    public void PlaceBlockAt(BlockItem block, int x, int y)
    {
        int addX = x % Chunk.size;
        int mX = (x - addX) / Chunk.size;
        int addY = y % Chunk.size;
        int mY = (y - addY) / Chunk.size;
        
        if (world.map[mX, mY].blockDatas[addX, addY].Title == "Air")
        {
            world.map[mX, mY].blockDatas[addX, addY] = block;
            MVVMBlock MVVMblock = Instantiate(MVVMBlockPrefab, new Vector2(x,y) + 0.5f * Vector2.one, Quaternion.identity);
            MVVMblock.block = block;
            ChunkGO Chunkgo = ChunkMap[new Vector2Int(mX, mY)];
            MVVMblock.chunkGO = Chunkgo;
            MVVMblock.transform.SetParent(Chunkgo.transform);
        }
    }

    public bool IsBlockCanBePlaced(int x, int y)
    {
        int addX = x % Chunk.size;
        int mX = (x - addX) / Chunk.size;
        int addY = y % Chunk.size;
        int mY = (y - addY) / Chunk.size;

        if (world.map[mX, mY].blockDatas[addX, addY].Id == -1)
        {
            return true;
        }
        return false;
    }

    public void PlaceBlockAt(int blockID, int x, int y)
    {
        int addX = x % Chunk.size;
        int mX = (x - addX) / Chunk.size;
        int addY = y % Chunk.size;
        int mY = (y - addY) / Chunk.size;

        if (world.map[mX, mY].blockDatas[addX, addY].Id == -1)
        {
            world.map[mX, mY].blockDatas[addX, addY] = database.FetchBlockItemByID(blockID);
            MVVMBlock MVVMblock = Instantiate(MVVMBlockPrefab, new Vector2(x, y) + 0.5f * Vector2.one, Quaternion.identity);
            MVVMblock.block = database.FetchBlockItemByID(blockID);
            ChunkGO Chunkgo = ChunkMap[new Vector2Int(mX, mY)];
            MVVMblock.chunkGO = Chunkgo;
            MVVMblock.transform.SetParent(Chunkgo.transform);
        }
    }

    public void SpawnItem(Item item, int x, int y)
    {
        GameObject go = Instantiate(DropItemPrefab, new Vector2(x, y), Quaternion.identity);
        go.GetComponent<DroppedItemAtWorld>().item = item;
    }
}
