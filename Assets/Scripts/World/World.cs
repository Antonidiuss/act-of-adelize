﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class World : ScriptableObject
{
    public string _name;
    public int _size;
    public float _seed;
    public Chunk[,] map;

    public World(string name, int size, float seed, Chunk[,] map)
    {
        _name = name;
        _size = size;
        _seed = seed;
        this.map = map;
    }
}
