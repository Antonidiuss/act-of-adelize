﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class GOArray
{
    [SerializeField]
    public MVVMBlock[] _gameObjects;

    // optionally some other fields
}

public class ChunkGO : MonoBehaviour
{
    public int _x, _y;
    
    [SerializeField]
    public List<GOArray> _list;

    public Chunk _chunk;


    // Start is called before the first frame update
    void Start()
    {
        LoadChunkData();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void LoadChunkData()
    {
        _x = _chunk._x;
        _y = _chunk._y;

        for (int x = 0; x < Chunk.size; x++)
        {
            for (int y = 0; y < Chunk.size; y++)
            {
                //try
                //{
                if (_chunk.blockDatas[x, y].Id != -1)
                {
                    MVVMBlock mVVMBlock = _list[y]._gameObjects[x].GetComponent<MVVMBlock>();
                    mVVMBlock.block = _chunk.blockDatas[x, y];
                    mVVMBlock._x = x;
                    mVVMBlock._y = y;
                }
                else
                {
                    Destroy(_list[y]._gameObjects[x].gameObject);
                }
                //}
                //catch (System.NullReferenceException e)
                //{
                //    Destroy(_list[y]._gameObjects[x].gameObject);
                //}
            }
        }
    }
}

