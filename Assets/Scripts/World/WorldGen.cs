﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGen
{
    public Block Grass, Dirt, Stone, Air;

    public List<List<BlockData>> blockMap;

    private bool initiated = false;
    ItemDatabase database;

    public void InitGen()
    {
        if (!initiated)
        {
            blockMap = new List<List<BlockData>>();
            database = GameObject.Find("Inventory").GetComponent<ItemDatabase>();
        }
    }

    public World GenerateWorld(string name, int size, float seed, float smoothness, float heightMultip, int heightAddition)
    {
        Chunk[,] chunks = new Chunk[size, size];

        BlockItem[,] blocks = new BlockItem[Chunk.size * size, Chunk.size * size];

        for (int i = 0; i < size * Chunk.size; i++)
        {
            int h = Mathf.RoundToInt(Mathf.PerlinNoise(seed, (i * i * Chunk.size) / smoothness) * heightMultip) + heightAddition;
            int GrassLevel = Random.Range(1, 1);
            int DirtLevel = Random.Range(2, 4);

            for (int j = 0; j < size * Chunk.size; j++)
            {
                
                if (j < h - DirtLevel - GrassLevel)
                {
                    blocks[i, j] = database.FetchBlockItemByID(4);

                    //blocks[i, j] = new BlockData(Stone, i, j, BlockLayout.main);
                }
                else if (j < h - GrassLevel)
                {
                    blocks[i, j] = database.FetchBlockItemByID(2);
                    //blocks[i, j] = new BlockData(Dirt, i, j, BlockLayout.main);
                }
                else if (j < h)
                {
                    blocks[i, j] = database.FetchBlockItemByID(3);
                    //blocks[i, j] = new BlockData(Grass, i, j, BlockLayout.main);
                }
                else
                {
                    blocks[i, j] = new BlockItem();
                    //blocks[i, j] = new BlockData(Air, i, j, BlockLayout.main);
                }

            }
        }



        for (int y = 0; y < size; y++)
        {
            for (int x = 0; x < size; x++)
            {
                chunks[x, y] = SliceMapToChunk(blocks, x, y);
                chunks[x, y]._x = x;
                chunks[x, y]._y = y;
            }
        }

        World world = new World(name, size, seed, chunks);

        return world;
    }

    public Chunk SliceMapToChunk(BlockItem[,] map, int x, int y)
    {
        Chunk chunk = new Chunk();
        chunk.blockDatas = new BlockItem[Chunk.size, Chunk.size];

        for (int _x = 0; _x < Chunk.size; _x++)
        {
            for (int _y = 0; _y < Chunk.size; _y++)
            {
                chunk.blockDatas[_x, _y] = map[x * Chunk.size + _x, y * Chunk.size + _y];
            }
        }

        return chunk;
    }

    //public Chunk ChunkGen(int x, int y, float seed, float smoothness, float heightMultip, int heightAddition)
    //{
    //    Chunk chunk = new Chunk();
    //    chunk._x = x;
    //    chunk._y = y;
    //    BlockItem[,] blocks = new BlockItem[Chunk.size, Chunk.size];
        
        

    //    for (int i = 0; i < Chunk.size; i++)
    //    {
    //        int h = Mathf.RoundToInt(Mathf.PerlinNoise(seed, (i * x * Chunk.size) / smoothness) * heightMultip) + heightAddition;

    //        for (int j = 0; j < Chunk.size; j++)
    //        {
    //            if (j < h - 4)
    //            {
    //                blocks[i, j] = new BlockData(Stone, x * Chunk.size + i, y * Chunk.size + j, BlockLayout.main);
    //            }
    //            else if (j < h - 1)
    //            {
    //                blocks[i, j] = new BlockData(Dirt, x * Chunk.size + i, y * Chunk.size + j, BlockLayout.main);
    //            }
    //            else if (j < h)
    //            {
    //                blocks[i, j] = new BlockData(Grass, x * Chunk.size + i, y * Chunk.size + j, BlockLayout.main);
    //            }
    //            else
    //            {
    //                blocks[i, j] = new BlockData(Air, x * Chunk.size + i, y * Chunk.size + j, BlockLayout.main);
    //            }

    //        }
    //    }

    //    chunk.blockDatas = blocks;

    //    return chunk;
    //}
}
