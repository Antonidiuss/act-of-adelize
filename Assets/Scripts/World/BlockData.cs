﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockData 
{
    public Block _block;
    public int _x, _y;
    public BlockLayout _layout;

    public BlockData(Block block, int x, int y, BlockLayout layout)
    {
        _block = block;
        _x = x;
        _y = y;
        _layout = layout;
    }
}
